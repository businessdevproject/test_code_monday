module.exports.get = async (req, res, next) => {
    try {
        const result = await CODEMONDAY.teacher.findAll();
        res.status(200).send({
            result: result
        });
    } catch (e) {
        next(e)
    }
};
module.exports.create = async (req, res, next) => {
    try {
        const result = await CODEMONDAY.teacher.create(req.body);
        res.status(200).send({
            result: result
        });
    } catch (e) {
        next(e)
    }
};