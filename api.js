const cors = require('cors');
const path = require('path');
const CodeMonday = require('./sdk');
const LISTENPORT = process.env.LISTENPORT;
const LANGUAGE = 'th-TH';
module.exports = (
    /**
     * @param {Auth} ATLAS
     * @param {CodeMonday} CODEMONDAY
     * @returns {Promise<express>}
     */
    async (CODEMONDAY) => {
        try {
            global.CODEMONDAY = CODEMONDAY;
            global.LOG_DB = true;

            process.on('SIGINT', function () {
                CODEMONDAY.shutdown();
                process.exit();
            });
            CODEMONDAY.setElasticSearchLog(CODEMONDAY.config.elasticsearch);
            await CODEMONDAY.init('test_code_monday', 'api');

            const options = {
                controllers: path.join(__dirname, './api'),
                loglevel: 'error',
                strict: true,
                router: true,
                validator: true,
                customErrorHandling: true,
                port: LISTENPORT,
                docs: {
                    apiDocs: '/api-doc',
                    apiDocsPrefix: '/test_code_monday',
                    swaggerUi: '/docs',
                    swaggerUiPrefix: '/test_code_monday',
                },
                expressJson: {
                    limit: '2mb'
                },
                expressUrlEncoded: {
                    limit: '2mb',
                    exended: true,
                }
            };

            const app = await CODEMONDAY.initApi(__dirname + '/api.yaml', options);

            //add default language as query param to each request
            app.use((req, res, next) => {
                req.language = req.query.language || LANGUAGE;
                next();
            });

            app.use(cors());

            return await CODEMONDAY.setApiRoutes();
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    }
)(new CodeMonday());

