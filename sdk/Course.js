"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Course {
    /**
     * @constructor
     * @param {Atlantic} Atlantic SDK
     */
    constructor(codeMonday) {
        this.codeMonday = codeMonday;
    }
    findAll() {
        return this.codeMonday.db.course.findAll({
            raw: true,
            include: [
                {
                    model: this.codeMonday.db.teacher,
                    as: "teachers",
                    attributes: ["id", "first_name", "last_name", "age"],
                    through: {
                        attributes: [],
                    }
                }
            ]
        }).then((courses) => {
            return JSON.parse(JSON.stringify(courses));
        }).catch((err) => {
            console.log(">> Error while retrieving course: ", err);
        });
    }
    create(data) {
        return this.codeMonday.db.course.create({
            course_name: data.course_name,
            course_description: data.course_description
        }).then(async (course) => {
            console.log(">> Created course: " + JSON.stringify(course, null, 2));
            if (data.teacher_ids) {
                let resTeacher = await new Promise(async (resolve, reject) => {
                    let teachers = [];
                    for (let teacher_id of data.teacher_ids) {
                        let result = await new Promise((resolve, reject) => {
                            this.codeMonday.db.teacher.findByPk(teacher_id).then((teacher) => {
                                if (!teacher) {
                                    console.log("teacher not found!");
                                    resolve(null);
                                }
                                else {
                                    course.addTeacher(teacher);
                                    console.log(`>> added course id=${course.id} to teacher id=${teacher.id}`);
                                    resolve(teacher);
                                }
                            });
                        });
                        if (result) {
                            teachers.push(result);
                        }
                    }
                    resolve(teachers);
                });
                return Object.assign(Object.assign({}, JSON.parse(JSON.stringify(course))), { teachers: resTeacher });
            }
            else {
                return course;
            }
        }).catch((err) => {
            console.log(">> Error while creating course: ", err);
        });
    }
}
exports.default = Course;
//# sourceMappingURL=Course.js.map