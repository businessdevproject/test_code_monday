module.exports = {
    GSB: {
        loan: {},
        deposit: {
            url: "https://sandbox.api.gsb.or.th/v1/bank-agent",
            app_id: "2359177f",
            app_key: "a6249c19adeafcc5391358fab33deb53"
        }
    },
    KYC: {
        url: "https://openapi-test.kasikornbank.com",
        cert: "www.termsabuyplus.com_2020.crt",
        private_key: "www.termsabuyplus.com_2020.key",
        consumer_key: "0LfADGnP5HCNPUBLfAnx0JP4dRYyjBhp",
        consumer_secret: "GAIAQaM4vDawSzZH",
        partner_id: "TSP0001",
        partner_secret: "557390a307edbd27dfef7312d7148719cd5dea6674d753ebc74a78ddff5af68c"
    },
    APLAY: {
        url: 'http://111.223.48.155/subs/apireceiver.php'
    },
    CIMB: {
        transfer: {
            url: "https://partner.cimbthai.com:443",
            customer_identity: "SATEGkd994QzkINnf987PlzK",
            passcode: "Vkl948DspKCpi928DqlRMfp216RgtHyB",
            client_id: "Kjz946BveVVsm833CadCGus348HmwSwO",
            client_secret: "Kmk733EwxOQir146KumJFws629ZonVkBBny507BgwWRpv588DfbCBvc648LlkIrD",
            client_ref_id: "20210208170536",
            public_key: "CCF0A4796F41A47E01FF9FF169C9E168C5C7E46F9159D33AABE314EF2F04C68C1AB688B933FAFFDB11251C0C771FEC59AC6EA22A98F9E550F4D392DFC0AE3F591ED3555BE3D121D5011DD8C9AC52CC165EC22A67EE7AF4A7EE724DC5B04DE86D296A6B36954BB93E2DA707E076A120621E41CED5F8A87396E80E352ED886C17FE4F970E8ACDE6C71C157F241918EE7EB36A64698C2A57C243E4DBD6D6BCF7DB6427CCA7322B9D5504F70F7F8A98F6084F37B0ED3D7B48DEF8C3FFAC52ECDF98F696E93346A73EAE631AF77E95CF25D5EAF658DBE6BE17FFAA3760D647028CF30E3DD21758B94F76BE5317CE36293E96FF7712087C9333367CF372FEF4E173433",
            exponent: "010001",
            channel: "LCCvCuExfUyJHJPxMFWggg",
            key64: 'k83YcbL2kMA06PWNWbyo+UbIVTkmURC8TFbsB5J69uI=',
            iv64: '9cW2b5NVd9qpkwCjJN4Ljw==',
            bank: {
                CIMBTRANSFER: {
                    bank_code: '022',
                },
                CIMBTRANSFER_KBANK: {
                    bank_code: '004',
                },
                CIMBTRANSFER_SCB: {
                    bank_code: '014',
                },
                CIMBTRANSFER_BBL: {
                    bank_code: '002',
                },
                CIMBTRANSFER_BAY: {
                    bank_code: '025',
                },
                CIMBTRANSFER_GSB: {
                    bank_code: '030',
                },
                CIMBTRANSFER_TMB: {
                    bank_code: '011',
                },
            }
        }
    },
    AIS_TOPUP_OPTION: {
        host: 'mediator.mpay.co.th',
        port: 443,
        path: '/mediator/mpayservice',
        method: 'POST',
        secureProtocol: 'TLSv1_2_method',
        rejectUnauthorized: false,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': 0
        }
    },
    airpay: {
        url: "http://api.is.airpay.in.th",
        secret: "nBi4tGZYPIdcvlewanu7hau9Y6BcoiD9"
    },
    scyllaPacific: {
        "contactPoints": [
            "prod-data1",
            "prod-data2",
            "prod-data3"
        ],
        "keyspace": "pacificdb",
        "username": "vdcapp",
        "password": "DFIpz7xEWog156ea0CWb",
        "localDataCenter": "DC1"
    },
    redis: {
        sentinels: [
            { host: "prod-redis", port: 26379 },
            { host: "prod-redis2", port: 26379 },
            { host: "prod-redis3", port: 26379 }
        ],
        name: "mymaster"
    },
    SITEID: 1,
    van: {
        url: "https://stg5.buzzebees.com",
        token: ".CulBx3KMfjsYWemh6FTHfV1BznNRkQAHDM913yXhHo2y2pQjgvw0do3pigP5QW681KCLQ3CKoG0izY9oOk5ZISM6fxxzoZuwNvDOm2VnWl84LsOTqnSYKhaXA1sFjPC2dX_SIeS9tRp0duQk5QtOBoP31njo3a35WaQ0iShlhPjeENIVd8-wAOm_6-G2buB7"
    },
    "cayman": {
        "url": "https://api-int.vdc.co.th",
        "authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ2ZGMuY28udGgiLCJuYW1lIjoicGFjaWZpYzIiLCJ0eXBlIjoic2l0ZSJ9.IHNFsVgVj6QbeipwyiB626u6W-OEdKws2xnbrXMllPk",
        "secret": "FKdt95Grx8h"
    },
    sbm: {
        url: "https://api.vdc.co.th/account",
    },
    message: {
        url: "https://api.vdc.co.th/message",
    },
    docsys: {
        url: "https://api.vdc.co.th/docsys/v1",
    },
    emerald: {
        url: "https://api.vdc.co.th/customer",
    },
    CLIENT_ID: "atlantic",
    CLIENT_SECRENT: "atlantic",
    AUTH_ENDPOINT: "https://api.vdc.co.th/auth",
    GSB_LOAN_ENDPOINT: "https://gateway.api.gsb.or.th/v1/loanpay",
    GSB_APP_ID: "28b52ceb",
    GSB_APP_KEY: "4f22266d4dc51ea9657e9dbfdfdfda42",
    URL_SC: "https://api.smartrms.net",
    URL_SERVER_BYTE: "http://byte.vdc.co.th/vdc/catalog/uploadMerchant/development/"
};
//# sourceMappingURL=production2.js.map