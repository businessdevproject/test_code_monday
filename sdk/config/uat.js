module.exports = {
    SBMB2C_TIMEOUT: 120000,
    kafka: {
        kafkaHost: 'uat-kafka1:9092',
        groupId: 'atlantic',
        fromOffset: 'latest',
        // Auto commit config
        autoCommit: true,
        autoCommitIntervalMs: 5000,
        // The max wait time is the maximum amount of time in milliseconds to block waiting if insufficient data is available at the time the request is issued, default 100ms
        fetchMaxWaitMs: 100,
        // This is the minimum number of bytes of messages that must be available to give a response, default 1 byte
        fetchMinBytes: 1,
        // The maximum bytes to include in the message set for this partition. This helps bound the size of the response.
        fetchMaxBytes: 1024 * 1024,
        // If set to 'buffer', values will be returned as raw buffer objects.
        encoding: 'utf8'
    },
    elasticsearch: {
        nodes: [
            'http://cent-elastic1:9200/',
            'http://cent-elastic2:9200/',
            'http://cent-elastic3:9200/'
        ],
        auth: {
            username: 'atlantic_api',
            password: '5s,X+P{jez/\'vE=P'
        },
        trace: true
    },
    TTL_ELASTIC_SEARCH_CONNECTING: 300000,
    KAFKAGROUP: 'gadget',
    KAFKATOPICS: ["atlantic.txn.add", "atlantic.txn.update", "atlantic.txn.delete"],
    scyllaAtlantic: {
        contactPoints: ["uat-data1"],
        keyspace: "atlanticdb",
        username: "vdcapp",
        password: "myStar1983",
        localDataCenter: "datacenter1"
    },
    GSB: {
        loan: {},
        deposit: {
            url: "https://sandbox.api.gsb.or.th/v1/bank-agent",
            app_id: "2359177f",
            app_key: "a6249c19adeafcc5391358fab33deb53"
        }
    },
    APLAY: {
        url: 'http://111.223.48.155/subs/apireceiver.php'
    },
    KYC: {
        url: "https://openapi-test.kasikornbank.com",
        cert: "www.termsabuyplus.com_2020.crt",
        private_key: "www.termsabuyplus.com_2020.key",
        consumer_key: "0LfADGnP5HCNPUBLfAnx0JP4dRYyjBhp",
        consumer_secret: "GAIAQaM4vDawSzZH",
        partner_id: "TSP0001",
        partner_secret: "557390a307edbd27dfef7312d7148719cd5dea6674d753ebc74a78ddff5af68c"
    },
    AIS_TOPUP_OPTION: {
        host: 'chillchill.ais.co.th',
        port: 8002,
        path: '/mediator/mpayservice',
        method: 'POST',
        secureProtocol: 'TLSv1_2_method',
        rejectUnauthorized: false,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': 0
        }
    },
    scyllaPacific: {
        contactPoints: ["uat-data1"],
        keyspace: "pacificdb",
        username: "vdcapp",
        password: "myStar1983",
        localDataCenter: "datacenter1"
    },
    redis: {
        host: "uat-svc1",
        port: 6379
    },
    SITEID: 5,
    van: {
        url: "https://stg5.buzzebees.com",
        token: ".CulBx3KMfjsYWemh6FTHfV1BznNRkQAHDM913yXhHo2y2pQjgvw0do3pigP5QW681KCLQ3CKoG0izY9oOk5ZISM6fxxzoZuwNvDOm2VnWl84LsOTqnSYKhaXA1sFjPC2dX_SIeS9tRp0duQk5QtOBoP31njo3a35WaQ0iShlhPjeENIVd8-wAOm_6-G2buB7"
    },
    airpay: {
        url: "http://is-api.test.airpay.in.th",
        secret: "nfv56uZddlx9SeoCoLDfHXrfh6Ig3d3d"
    },
    cayman: {
        url: "https://api-uat.vdc.co.th/caymansbm",
        authorization: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ2ZGMuY28udGgiLCJuYW1lIjoicGFjaWZpYzIiLCJ0eXBlIjoic2l0ZSJ9.IHNFsVgVj6QbeipwyiB626u6W-OEdKws2xnbrXMllPk",
        secret: "FKdt95Grx8h"
    },
    atlantic: {
        url: "https://api-uat.vdc.co.th/atlantic",
    },
    caribbean: {
        url: "https://api-uat.vdc.co.th/caribbean",
    },
    sbm: {
        url: "https://api-uat.vdc.co.th/account",
    },
    message: {
        url: "https://api-uat.vdc.co.th/message",
    },
    docsys: {
        url: "https://api-uat.vdc.co.th/docsys",
    },
    emerald: {
        url: "https://api-uat.vdc.co.th/customer",
    },
    CLIENT_ID: "atlantic",
    CLIENT_SECRENT: "atlantic",
    AUTH_ENDPOINT: "https://api-uat.vdc.co.th/auth",
    GSB_LOAN_ENDPOINT: "https://sandbox.api.gsb.or.th/v1/loanpay",
    GSB_APP_ID: "28b52ceb",
    GSB_APP_KEY: "026a989976f6d26ddd9a4e0262a428e2",
    URL_SC: "https://uat.smartrms.net",
    AMLO: {
        URL_CITIZEN: "https://api-uat.vdc.co.th/customer/v1/customer",
        URL_CUSTOMER: "https://api-uat.vdc.co.th/customer/v2/customer",
        URL_AUTHEN: "https://api-uat.vdc.co.th/auth/v1/token",
        CREDENTIALS: {
            "client_id": "portal",
            "client_secret": "x",
            "grant_type": "client_credentials"
        }
    },
    URL_SERVER_BYTE: "https://byte-uat.vdc.co.th/vdc/catalog/uploadMerchant/development/",
    CIMB: {
        deposit: {
            url: "https://delta.cimbthai.com",
            customer_identity: "g47uEzIly1Z80wsIsLU1Ta7IF95wcexQ",
            passcode: "uH5lZmCeLxBhaARwi8bp93tCYR1ZpsdiAeqscIaaf4tY7b1D34iNrOQKHIsAPwRs",
            client_id: "rcltYIsosfCzKJweiPEQhkXG36VYSc2b",
            client_secret: "ps38yPPwRIGVcTbZbS0xObk2ZTjyvEDFVDIZyJCfaIm5TXILfj6A7qppf1jrUTTq",
            client_ref_id: "20210113162703",
            public_key: "C7AF407F04E311D4269471021E01897B62F8457E54B93D7E655C192AB60E991C98202E0F63278D971A35ACDFD5A90BC21A6731DC8CFA0A07211E519C933B9BC63F9EE774DEF8CAF3E9186C5AF4FE5C8CD03FD3641682A0FF0EF1B08571B30B51759AB81A30B9BE6A28523BFD6C5060E3AFE95A1B25355D67839654F64451F4D59BF8CB4DF2121F1E1DB79A74CB1FE284308495389F7889427BB4E9C0259E9C0402D110737A82CE966FA0B8E75CD8960AD4C1854D329315F37D60B182AA1B10958865CDFCC0DFEE1C399BE43FBFD2140F26A5F1FD9645B7BFEA1F87D0141766B7A44ADB86A88C5DD980EF1CBB5ED5E0436E996F99D3737668F1764B2EDDE17E1F",
            exponent: "010001",
            channel: "LJed6qZHB0aD/oNC6am3+A",
            key64: 'CWTP7iaaakINnoMA21oKsFdOsPqFP+LdLIZ75P61rQg=',
            iv64: '77v15R1i6pJgaIhEsh2RgA==',
            sender_id: 'SenderID',
            sender_name: 'K',
            bank: {
                CIMBTRANSFER: {
                    bank_code: '022'
                },
                CIMBTRANSFER_KBANK: {
                    bank_code: '004'
                },
                CIMBTRANSFER_SCB: {
                    bank_code: '014'
                },
                CIMBTRANSFER_BBL: {
                    bank_code: '002'
                },
                CIMBTRANSFER_BAY: {
                    bank_code: '025'
                },
                CIMBTRANSFER_GSB: {
                    bank_code: '030'
                }
            }
        }
    }
};
//# sourceMappingURL=uat.js.map