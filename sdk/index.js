"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sdk = require('@sabuytech/libsbt');
const _ = require('lodash');
const config = require('./config/' + process.env.NODE_ENV);
const db = require("../models");
const Teacher_1 = require("./Teacher");
const Course_1 = require("./Course");
/**
 * @class Atlantic
 * @extends Sdk
 */
class CodeMonday extends Sdk {
    /**
     * @constructor
     */
    constructor() {
        super();
        this.config = config;
    }
    /**
     * Init Sale Class
     * @returns {Promise<Atlantic>}
     */
    async init(origin, facility) {
        await super.init(origin, facility);
        this.db = db;
        this.teacher = await new Teacher_1.default(this);
        this.course = await new Course_1.default(this);
        return this;
    }
    shutdown() {
        super.shutdown();
        this.van = null;
        this.db.shutdown();
    }
}
exports.default = CodeMonday;
module.exports = CodeMonday;
//# sourceMappingURL=index.js.map