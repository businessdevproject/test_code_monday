"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Teacher {
    /**
     * @constructor
     * @param {Atlantic} Atlantic SDK
     */
    constructor(codeMonday) {
        this.codeMonday = codeMonday;
    }
    findAll() {
        return this.codeMonday.db.teacher.findAll({
            raw: true,
            include: [
                {
                    model: this.codeMonday.db.course,
                    as: "courses",
                    attributes: ["id", "course_name", "course_description"],
                    through: {
                        attributes: [],
                    }
                }
            ]
        }).then((teachers) => {
            return JSON.parse(JSON.stringify(teachers));
        }).catch((err) => {
            console.log(">> Error while retrieving teacher: ", err);
        });
    }
    create(data) {
        return this.codeMonday.db.teacher.create({
            first_name: data.first_name,
            last_name: data.last_name,
            age: data.age
        }).then(async (teacher) => {
            console.log(">> Created teacher: " + JSON.stringify(teacher, null, 2));
            if (data.course_ids) {
                let resCourses = await new Promise(async (resolve, reject) => {
                    let courses = [];
                    for (let course_id of data.course_ids) {
                        let result = await new Promise((resolve, reject) => {
                            this.codeMonday.db.course.findByPk(course_id).then((course) => {
                                if (!course) {
                                    console.log("course not found!");
                                    resolve(null);
                                }
                                else {
                                    teacher.addCourse(course);
                                    console.log(`>> added teacher id=${teacher.id} to course id=${course.id}`);
                                    resolve(course);
                                }
                            });
                        });
                        if (result) {
                            courses.push(result);
                        }
                    }
                    resolve(courses);
                });
                return Object.assign(Object.assign({}, JSON.parse(JSON.stringify(teacher))), { courses: resCourses });
            }
            else {
                return teacher;
            }
        }).catch((err) => {
            console.log(">> Error while creating teacher: ", err);
        });
    }
}
exports.default = Teacher;
//# sourceMappingURL=Teacher.js.map