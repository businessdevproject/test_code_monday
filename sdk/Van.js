"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config = require('./config/' + process.env.NODE_ENV);
class Van {
    /**
     * @constructor
     * @param {Atlantic} Atlantic SDK
     */
    constructor(codeMonday) {
        this.codeMonday = codeMonday;
    }
}
exports.default = Van;
//# sourceMappingURL=Van.js.map