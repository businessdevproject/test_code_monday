const os = require('os');
const config = require('./config/' + os.userInfo().username);
process.env.NODE_ENV = config.NODE_ENV;
process.env.LISTENPORT = config.LISTENPORT;
const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const {spawn} = require('child_process');
const path = require('path');

// Configure chai
chai.use(chaiHttp);
chai.should();


describe('API Server', function () {
    let server;
    this.timeout(20000);

    after(() => {
        server.kill('SIGINT');
    });

    it('should start server',async () => {

        await new Promise((resolve) => {
            server = spawn('node', [path.resolve(__dirname + '/../', 'api.js')], {env: {LISTENPORT: process.env.LISTENPORT, NODE_ENV: process.env.NODE_ENV, PATH: process.env.PATH}});
            server.stdout.on('data', function (data) {
                let line = data.toString();
                console.log('SERVER:', line);
                if (line.indexOf('port') !== -1) {
                    resolve();
                }
            });
            server.stderr.on('data', function (data) {
                process.stderr.write(data);
            });
        });
    });
});

