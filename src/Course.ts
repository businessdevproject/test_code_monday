import CodeMonday from "./index";
import IDefaultDao from "./interface/DefaultInterface";

export default class Course implements IDefaultDao {

    private codeMonday: CodeMonday;

    /**
     * @constructor
     * @param {Atlantic} Atlantic SDK
     */
    constructor(codeMonday: CodeMonday) {
        this.codeMonday = codeMonday;
    }

    public findAll(): any {
        return this.codeMonday.db.course.findAll({
            raw: true,
            include: [
                {
                    model: this.codeMonday.db.teacher,
                    as: "teachers",
                    attributes: ["id", "first_name", "last_name", "age"],
                    through: {
                        attributes: [],
                    }
                }
            ]
        }).then((courses: any) => {
            return JSON.parse(JSON.stringify(courses));
        }).catch((err: any) => {
            console.log(">> Error while retrieving course: ", err);
        });
    }

    public create(data: any): any {
        return this.codeMonday.db.course.create({
            course_name: data.course_name,
            course_description: data.course_description
        }).then(async (course: any) => {
            console.log(">> Created course: " + JSON.stringify(course, null, 2));
            if(data.teacher_ids){
                let resTeacher: any[] = await new Promise(async (resolve, reject) => {
                    let teachers: any[] = [];
                    for(let teacher_id of data.teacher_ids){
                        let result: any =  await new Promise((resolve, reject) => {
                            this.codeMonday.db.teacher.findByPk(teacher_id).then((teacher: any) => {
                                if (!teacher) {
                                    console.log("teacher not found!");
                                    resolve(null);
                                }else{
                                    course.addTeacher(teacher);
                                    console.log(`>> added course id=${course.id} to teacher id=${teacher.id}`);
                                    resolve(teacher);
                                }
                            });
                        });
                        if(result){
                            teachers.push(result);
                        }
                    }
                    resolve(teachers);
                });
                return {...JSON.parse(JSON.stringify(course)), ...{teachers: resTeacher}};
            }else{
                return course;
            }
        }).catch((err: any) => {
            console.log(">> Error while creating course: ", err);
        });
    }
}