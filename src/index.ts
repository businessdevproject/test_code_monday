const Sdk = require('@sabuytech/libsbt');
const _ = require('lodash');
const config = require('./config/' + process.env.NODE_ENV);
const db = require("../models");
import Teacher from './Teacher';
import Course from './Course';
/**
 * @class Atlantic
 * @extends Sdk
 */
export default class CodeMonday extends Sdk {

    /**
     * @constructor
     */
    constructor() {
        super();
        this.config = config;
    }

    /**
     * Init Sale Class
     * @returns {Promise<Atlantic>}
     */
    async init(origin: string, facility: string) {
        await super.init(origin, facility);
        this.db = db;
        this.teacher = await new Teacher(this);
        this.course = await new Course(this);

        return this;
    }

    shutdown() {
        super.shutdown();
        this.van = null;
        this.db.shutdown();
    }
}

module.exports = CodeMonday;
