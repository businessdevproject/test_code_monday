import CodeMonday from "./index";
import IDefaultDao from "./interface/DefaultInterface";

export default class Teacher implements IDefaultDao {

    private codeMonday: CodeMonday;

    /**
     * @constructor
     * @param {Atlantic} Atlantic SDK
     */
    constructor(codeMonday: CodeMonday) {
        this.codeMonday = codeMonday;
    }

    public findAll(): any {
        return this.codeMonday.db.teacher.findAll({
            raw: true,
            include: [
                {
                    model: this.codeMonday.db.course,
                    as: "courses",
                    attributes: ["id", "course_name", "course_description"],
                    through: {
                        attributes: [],
                    }
                }
            ]
        }).then((teachers: any) => {
            return JSON.parse(JSON.stringify(teachers));
        }).catch((err: any) => {
            console.log(">> Error while retrieving teacher: ", err);
        });
    }

    public create(data: any): any {
        return this.codeMonday.db.teacher.create({
            first_name: data.first_name,
            last_name: data.last_name,
            age: data.age
        }).then(async (teacher: any) => {
            console.log(">> Created teacher: " + JSON.stringify(teacher, null, 2));
            if(data.course_ids){
                let resCourses: any[] = await new Promise(async (resolve, reject) => {
                    let courses: any[] = [];
                    for(let course_id of data.course_ids){
                        let result: any =  await new Promise((resolve, reject) => {
                            this.codeMonday.db.course.findByPk(course_id).then((course: any) => {
                                if (!course) {
                                    console.log("course not found!");
                                    resolve(null);
                                }else{
                                    teacher.addCourse(course);
                                    console.log(`>> added teacher id=${teacher.id} to course id=${course.id}`);
                                    resolve(course);
                                }
                            });
                        });
                        if(result){
                            courses.push(result);
                        }
                    }
                    resolve(courses);
                });
                return {...JSON.parse(JSON.stringify(teacher)), ...{courses: resCourses}};
            }else{
                return teacher;
            }
        }).catch((err: any) => {
            console.log(">> Error while creating teacher: ", err);
        });
    }
}