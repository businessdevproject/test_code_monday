module.exports = (sequelize, DataTypes) => {
    const Teacher = sequelize.define("teacher", {
        first_name: {
            type: DataTypes.STRING,
        },
        last_name: {
            type: DataTypes.STRING,
        },
        age: {
            type: DataTypes.INTEGER,
        },
    });

    return Teacher;
};