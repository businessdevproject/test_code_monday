const config = require("../sdk/config/" + process.env.NODE_ENV);

const Sequelize = require("sequelize");
const sequelize = new Sequelize(config.mysqlDb.DB, config.mysqlDb.USER, config.mysqlDb.PASSWORD, {
    host: config.mysqlDb.HOST,
    dialect: config.mysqlDb.dialect,
    operatorsAliases: false,
    logging: console.log,
    pool: {
        max: config.mysqlDb.pool.max,
        min: config.mysqlDb.pool.min,
        acquire: config.mysqlDb.pool.acquire,
        idle: config.mysqlDb.pool.idle,
    },
});
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.teacher = require("./teacher.model.js")(sequelize, Sequelize);
db.course = require("./course.model.js")(sequelize, Sequelize);

db.teacher.belongsToMany(db.course, {
    through: "teacher_course",
    as: "courses",
    foreignKey: "teacher_id",
});
db.course.belongsToMany(db.teacher, {
    through: "teacher_course",
    as: "teachers",
    foreignKey: "course_id",
});

// fix for test
db.sequelize.sync({ force: true }).then(() => {
    console.log("Drop and re-sync db.");
}).catch((e) =>{
    console.error('error',e)
});
module.exports = db;