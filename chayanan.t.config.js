module.exports = {
  apps : [{
    "node_args": ["--inspect=172.20.8.6:14146"],
    name      : 'test_code_monday',
    script    : 'api.js',
    log_date_format: "YYYY-MM-DD HH:mm:ss",
    merge_logs: true,
    "env": {
      "NODE_ENV": "development",
      "LISTENPORT": 14046
    }
  }]
};
